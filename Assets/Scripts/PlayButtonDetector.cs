﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayButtonDetector : APBehaviour
{
    public DisasterController disasterController;
    #region ALL UNITY FUNCTIONS

    float tapStartTime, tapEndTime;
    float tapMaximumDuration = 0.25f;
    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    void Update()
    {
        //if (gameState.Equals(GameState.GAME_INITIALIZED) && Input.GetMouseButtonDown(0))
        //{
        //    gameManager.ChangeGameState(GameState.GAME_PLAY_STARTED);
        //    gameState = GameState.GAME_PLAY_STARTED;
        //}

        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void FixedUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void LateUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    private void OnMouseDown()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

        tapStartTime = Time.time;
    }

    private void OnMouseUp()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

        tapEndTime = Time.time;

        if ((tapEndTime - tapStartTime) <= tapMaximumDuration)
        {
            disasterController.OnPlayButtonClick();
        }
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS


    #endregion ALL SELF DECLEAR FUNCTIONS

}
