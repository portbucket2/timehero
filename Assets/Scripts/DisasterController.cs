﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class DisasterController : APBehaviour
{
    public RuntimeAnimatorController animatorController;
    public LevelsAsset levelsAsset;
    public Slider animationFrameSlider;
    public Transform handSliderDragger, dragSliderTextObj, dragThePuddleTextObj, tooLateTextObj;
    public Transform sliderPauseIcon, sliderPlayIcon;

    Animator anim;
    LevelAsset currentLeveAsset;
    Obstacle[] obstacles;
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();
        anim = GetComponent<Animator>();
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    void Update()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

        if(anim.speed == 0)
            anim.Play("CurrentState", 0, animationFrameSlider.value);
        else
        {
            animationFrameSlider.value = anim.GetCurrentAnimatorStateInfo(0).normalizedTime;
        }
    }

    void FixedUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void LateUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnInitialDisasterShown()
    {
        base.OnInitialDisasterShown();

        anim.speed = 0f;
        animationFrameSlider.value = 1;
        handSliderDragger.gameObject.SetActive(gameplayData.currentLevelNumber == 0);
        dragSliderTextObj.gameObject.SetActive(true);
        tooLateTextObj.gameObject.SetActive(false);
    }

    public override void OnGameStart()
    {
        base.OnGameStart();

        tooLateTextObj.gameObject.SetActive(false);
    }

    public override void OnGameOver()
    {
        base.OnGameOver();

        animationFrameSlider.gameObject.SetActive(false);
        SetThisClipToGamePlay(currentLeveAsset.successClip);
        anim.Play("CurrentState", 0, 0);
        anim.speed = 1;
    }

    public override void OnGameInitializing()
    {
        base.OnGameInitializing();

        currentLeveAsset = levelsAsset.allLevelAssets[gameManager.GetModedLevelNumber()];

        GameObject go = Instantiate(currentLeveAsset.levelEnvironment, Vector3.zero, Quaternion.identity, transform);
        go.name = currentLeveAsset.levelEnvironment.name;

        LevelHolder levelHolder = go.GetComponent<LevelHolder>();
        Camera.main.transform.DOMove(levelHolder.cameraPoint.position, ConstantManager.DEFAULT_ANIMATION_TIME);
        Camera.main.transform.DORotate(levelHolder.cameraPoint.eulerAngles, ConstantManager.DEFAULT_ANIMATION_TIME);

        anim = go.GetComponent<Animator>();
        anim.runtimeAnimatorController = animatorController;
        anim.speed = 0f;
        animationFrameSlider.value = 0;
        SetSliderPauseIcon(true);
        anim.Play("CurrentState", 0, animationFrameSlider.value);

        SetThisClipToGamePlay(currentLeveAsset.failClip);

        obstacles = transform.GetComponentsInChildren<Obstacle>();
        foreach (Obstacle item in obstacles)
        {
            gameManager.totalGivenTask++;
            item.Initialize(this, anim);
        }

        APTools.functionManager.ExecuteAfterSecond(ConstantManager.DEFAULT_ANIMATION_TIME + 0.25f, () =>
        {
            anim.speed = 1f;
        });
    }

    private void OnMouseDown()
    {
        Debug.LogError("Yes Down");
    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    public void OnPlayButtonClick()
    {
        SetSliderPauseIcon(true);
        anim.speed = 1f;
    }

    public void OnSliderClick()
    {
        anim.speed = 0;

        handSliderDragger.gameObject.SetActive(false);
        dragSliderTextObj.gameObject.SetActive(false);
        SetSliderPauseIcon(false);
    }

    void SetSliderPauseIcon(bool pause)
    {
        sliderPauseIcon.gameObject.SetActive(pause);
        sliderPlayIcon.gameObject.SetActive(!pause);
    }

    public void OnElemenateObstacle()
    {
        gameManager.OnCompleteATask();
    }

    public void SetThisClipToGamePlay(AnimationClip animationClip)
    {
        AnimatorOverrideController aoc = new AnimatorOverrideController(anim.runtimeAnimatorController);
        var anims = new List<KeyValuePair<AnimationClip, AnimationClip>>();
        foreach (var a in aoc.animationClips)
            anims.Add(new KeyValuePair<AnimationClip, AnimationClip>(a, animationClip));
        aoc.ApplyOverrides(anims);
        anim.runtimeAnimatorController = aoc;
    }
    #endregion ALL SELF DECLEAR FUNCTIONS

}
