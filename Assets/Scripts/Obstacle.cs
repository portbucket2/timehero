﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class Obstacle : APBehaviour
{
    public Transform trashingPoint;
    public GameObject ground;
    public float snappingDistance = 3f;

    DisasterController disasterController;
    bool dragging, isInitialDisasterShown;
    float inputAcceptingDuration;
    Material trasingObjMaterial;
    Renderer[] obstacleRenderers, trashingRendrers;
    Vector3 initialPosition;

    Animator anim;
    AnimatorClipInfo[] animatorClipInfo;
    AnimatorStateInfo animationState;
    Coroutine blink;

    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();

        initialPosition = transform.position;
        trashingPoint.gameObject.SetActive(false);
        obstacleRenderers = GetComponentsInChildren<Renderer>();
        trashingRendrers = trashingPoint.GetComponentsInChildren<Renderer>();

        if (obstacleRenderers.Length > 0)
        {
            trasingObjMaterial = Resources.Load<Material>("TrashingObjectMaterial");
            for (int i = 0; i < trashingRendrers.Length; i++)
            {
                trashingRendrers[i].material = trasingObjMaterial;
            }
        }
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        Registar_For_Input_Callback();
    }

    void Update()
    {        
        if (animatorClipInfo[0].clip.length * anim.GetCurrentAnimatorStateInfo(0).normalizedTime / animatorClipInfo[0].clip.frameRate
            < inputAcceptingDuration)
        {
            if (blink == null && isInitialDisasterShown)
            {
                blink = StartCoroutine(Blink());

            }
            disasterController.dragThePuddleTextObj.gameObject.SetActive(gameplayData.currentLevelNumber == 0);
            disasterController.tooLateTextObj.gameObject.SetActive(false);

        }
        else
        {
            trashingPoint.gameObject.SetActive(false);
            disasterController.dragThePuddleTextObj.gameObject.SetActive(false);

            if (blink != null)
            {
                StopCoroutine(blink);
                blink = null;
            }
        }

        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;
    }

    void FixedUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

        if (dragging)
        {
            Vector3 newPosition = ground == null? APTools.mathManager.GetWorldTouchPosition() : APTools.mathManager.GetWorldTouchPosition(ground);
            newPosition.y = transform.position.y;

            transform.position =  newPosition;
        }

        if(transform.DistanceFrom(trashingPoint) <= snappingDistance)
        {
            OnElemenateObstacle();
        }
    }

    void LateUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    private void OnMouseDown()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;
        //Debug.Log(animatorClipInfo[0].clip.length * anim.GetCurrentAnimatorStateInfo(0).normalizedTime / animatorClipInfo[0].clip.frameRate);

        // checking if object is interecting after disaster.
        if (animatorClipInfo[0].clip.length * anim.GetCurrentAnimatorStateInfo(0).normalizedTime / animatorClipInfo[0].clip.frameRate
            > inputAcceptingDuration)
        {
            //Debug.LogError("Interecting with object after disaster at animation time: " + animatorClipInfo[0].clip.length * anim.GetCurrentAnimatorStateInfo(0).normalizedTime / animatorClipInfo[0].clip.frameRate);
            disasterController.tooLateTextObj.gameObject.SetActive(true);
            APTools.functionManager.ExecuteAfterSecond(2, () =>
            {
                disasterController.tooLateTextObj.gameObject.SetActive(false);
            });
            return;
        }

        dragging = true;
    }

    private void OnMouseUp()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

        dragging = false;
        transform.position = initialPosition;
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnInitialDisasterShown()
    {
        base.OnInitialDisasterShown();

        isInitialDisasterShown = true;
    }

    public override void OnTapStart(Vector3 tapOnWorldSpace)
    {
        base.OnTapStart(tapOnWorldSpace);
    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    IEnumerator Blink()
    {
        trashingPoint.gameObject.SetActive(!trashingPoint.gameObject.activeSelf);
        yield return new WaitForSeconds(0.25f);
        blink = null;
    }


    public void SetInputAcceptingDuration(float inputAcceptingDuration)
    {
        this.inputAcceptingDuration = inputAcceptingDuration;
    }

    public void OnElemenateObstacle()
    {
        dragging = false;
        transform.DOMove(trashingPoint.position, ConstantManager.DEFAULT_ANIMATION_TIME / 5).OnComplete(()=> {

            trashingPoint.gameObject.SetActive(false);
            disasterController.OnElemenateObstacle();
        });
    }

    public void Initialize(DisasterController disasterController, Animator anim)
    {
        this.disasterController = disasterController;
        this.anim = anim;
        animatorClipInfo = anim.GetCurrentAnimatorClipInfo(0);
        animationState = anim.GetCurrentAnimatorStateInfo(0);
    }

    #endregion ALL SELF DECLEAR FUNCTIONS

}
