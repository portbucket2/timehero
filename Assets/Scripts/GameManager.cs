﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

#if AP_GAMEANALYTICS_SDK_INSTALLED
using GameAnalyticsSDK;
#endif

public class GameManager : APManager
{
    public static Action OnInitialDisasterShown;

    public virtual void InitialDisasterShown()
    {
        OnInitialDisasterShown?.Invoke();
    }

    public override void Awake()
    {
        base.Awake();
        ChangeGameState(GameState.NONE);
    }

    public override void None()
    {
        base.None();
        ChangeGameState(GameState.GAME_DATA_LOADED);
    }

    public override void GameDataLoad()
    {
        base.GameDataLoad();
        ChangeGameState(GameState.GAME_INITIALIZED);
    }

    public override void GameInitialize()
    {
        base.GameInitialize();

        gamePlayUI.SetBool("Hide", false);
    }

    public override void GameStart()
    {
        base.GameStart();

        gameCustomUI.SetBool("Hide", false);

#if AP_GAMEANALYTICS_SDK_INSTALLED
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "World01", "Level " + (gameplayData.currentLevelNumber + 1));
#endif
    }

    public override void GameOver()
    {
        gameplayData.gameEndTime = Time.time;
        OnGameOver?.Invoke();

        gamePlayUI.SetBool("Hide", true);
        if (gameplayData.isGameoverSuccess)
        {
            gameplayData.currentLevelNumber++;
#if AP_GAMEANALYTICS_SDK_INSTALLED
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "World01", "Level " + gameplayData.currentLevelNumber);
#endif
        }
        else
        {
#if AP_GAMEANALYTICS_SDK_INSTALLED
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, "World01", "Level " + (gameplayData.currentLevelNumber + 1));
#endif
        }

        SaveGame();
    }

    public void OnAnimationComplete()
    {
        if (gameplayData.isGameoverSuccess)
        {
            gameSuccessUI.SetBool("Hide", false);
            gameOverEffect.Play();
            PlayThisSoundEffect(gameWinAudioClip);
        }
        else
        {
            gameFaildUI.SetBool("Hide", false);
        }

    }

}
