﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName ="LevelAssetsHolder", menuName = "LevelAssets/Create")]
public class LevelsAsset: ScriptableObject
{
    public LevelAsset[] allLevelAssets;
}

[Serializable]
public class LevelAsset
{
    public GameObject levelEnvironment;
    public AnimationClip successClip;
    public AnimationClip failClip;
}
