﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent( typeof(Animator))]
public class LevelHolder : APBehaviour
{
    public Transform cameraPoint;
    public float inputAcceptingDuration;

    Animator anim;
    List<Obstacle> obstacles;
    bool isInitialDisasterShown;

    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();

        anim = GetComponent<Animator>();
        obstacles = GetComponentsInChildren<Obstacle>().ToList();

        for (int i = 0; i < obstacles.Count; i++)
        {
            obstacles[i].SetInputAcceptingDuration(inputAcceptingDuration);
        }
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    void Update()
    {
        //if (gameState.Equals(GameState.GAME_INITIALIZED) && Input.GetMouseButtonDown(0))
        //{
        //    gameManager.ChangeGameState(GameState.GAME_PLAY_STARTED);
        //    gameState = GameState.GAME_PLAY_STARTED;
        //}

        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void FixedUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void LateUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnGameInitializing()
    {
        base.OnGameInitializing();

    }

    public override void OnInitialDisasterShown()
    {
        base.OnInitialDisasterShown();

        if (!isInitialDisasterShown)
        {
            isInitialDisasterShown = true;
            gameManager.ChangeGameState(GameState.GAME_PLAY_STARTED);
            ((GameManager)gameManager).InitialDisasterShown();
            return;
        }
    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    public void OnAnimationComplete()
    {
        ((GameManager)gameManager).OnAnimationComplete();
    }

    #endregion ALL SELF DECLEAR FUNCTIONS

}
