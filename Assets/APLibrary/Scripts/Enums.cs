﻿public enum LoadSceneType {

    LOAD_BY_NAME,
    LOAD_BY_INDEX
}

public enum GameState
{
    NONE, // This is a whistler to all APBehaviour that APManager just created.
    GAME_DATA_LOADED, // Game wii start loading gameplay existing/new data in this state, this state will call once in a scene
    GAME_INITIALIZED, // After loading gameplay data this state will be called, Game will be ready to play
    GAME_PLAY_STARTED,
    GAME_PLAY_ENDED,
    GAME_PLAY_PAUSED,
    GAME_PLAY_UNPAUSED
}

public enum AcceptedValueType
{
    BOTH,
    ONLY_POSITIVE,
    ONLY_NEGETIVE,
    NONE
}

public enum TappingType
{
    NONE,
    TAP_START,
    TAP_END,
    TAP_N_HOLD
}

public enum SwippingType
{
    SWIP_UP,
    SWIP_DOWN,
    SWIP_LEFT,
    SWIP_RIGHT
}